<?php

/**
 * @file
 *   Horrid hack, but ... out the door.
 *
 *   For all the things required making a custom pane/block in the first place would have made more sense.
 *
 * @ingroup views_templates
 */
$nid = $row->node_og_membership_nid;
$uid = (int) $view->args[0];
?>
<h3><?php print $fields['title']->content; unset($fields['title']) ?></h3>

<div class="membership-roles">
<?php 
$roles = og_get_user_roles('node', $nid, $uid);
if ($uid == $row->node_og_membership_uid) {
  array_unshift($roles, t('founder'));
}
array_walk($roles, function (&$role, $rid) {
  $role = check_plain($role);
});
print implode(', ', $roles);
?>
</div>

<ul>
  <li><?php print l(t('Group page'), "/node/$nid"); ?></li>
<?php if (og_user_access('node', $nid, 'administer group')): ?>
  <li><?php print l(t('Administer events'), "/node/$nid/moderation"); ?></li>
  <li><?php print l(t('Administer members'), "/node/$nid/group"); ?></li>
<?php endif; ?>
  <li><?php print "<a href=\"#links-$nid\">" . t('Links to show event content elsewhere') . "</a>"; ?>
    <dl class="group-event-links" id="links-<?php print $nid; ?>">
      <dt><?php print t("calendar (ical) feed"); ?></dt>
        <dd><?php print url("/ical/node/$nid", ['absolute' => true]); ?></dd>
      <dt><?php print t('link for website <a href="/en/blog/2018/08/events-your-website-easy-way">javascript</a> or <a href="https://wordpress.org/plugins/squat-radar-calendar-integration/">wordpress plugin</a>'); ?></dt>
        <dd><?php print url("/events/group/$nid", ['absolute' => true]); ?></dd>
      <dt><?php print t('<a href="https://0xacab.org/radar/drupal-make/-/wikis/api/1.2/notes">api url</a> for computer readable json'); ?></dt>
        <dd><?php print url("/api/1.2/search/events.json", ['absolute' => true, 'query' => ['facets' => ['group' => [$nid]]]]); ?></dd>
    </dl>
  </li>
</ul>
