<?php

/**
 * @file
 * Implementation of hook_panels_layouts
 */
function at_radar_three_brick_panels_layouts() {
  $items['three_brick'] = array(
    'title'    => t('AT three column brick'),
    'category' => t('AT Responsive Panels - 3 column'),
    'icon'     => 'three_brick.png',
    'theme'    => 'three_brick',
    'admin css' => 'three_brick.admin.css',
    'theme arguments' => array('id', 'content'),
    'regions' => array(
      'three_brick_top'         => t('Top (conditional)'),
      'three_brick_left_above'  => t('Left above'),
      'three_brick_center_above' => t('Center above'),
      'three_brick_right_above' => t('Right above'),
      'three_brick_middle'      => t('Middle (conditional)'),
      'three_brick_left_below'  => t('Left below'),
      'three_brick_right_below' => t('Right below'),
      'three_brick_bottom'      => t('Bottom (conditional)'),
    ),
    // AT Core
    'type' => 'three',
    'options' => array(
      'three-brick' => 'default',
      'three-brick-stack' => 'stack',
    ),
    'styles' => array(
      'three-brick' => array(
        'css' => array(
          'fn' => array('.three-brick > .panel-row' => 'float:none'),
          '33' => array('.three-brick > .panel-row > .region' => 'width:33%'),
          '66' => array('.three-brick > .panel-row > div.region-three-brick-right-below' => 'width:66%'),
        ),
      ),
      'three-brick-stack' => array(
        'css' => array(
          'fdw' => array('.three-brick > .panel-row > .region' => 'float:none;display:block;width:100%;clear:both'),
        ),
      ),
    ),
  );

  return $items;
}

/**
 * Preprocess variables for three-brick.tpl.php
 */
function template_preprocess_three_brick(&$vars) {
  $vars['panel_prefix'] = '';
  $vars['panel_suffix'] = '';
}
