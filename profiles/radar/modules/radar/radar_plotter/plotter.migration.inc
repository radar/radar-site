<?php

/**
 * Plotter data import classes.
 */

class RadarMigrateListDrupal7NodeServicesJSON extends MigrateListJSON {
  /**
   * {@inheritdoc}
   */
  protected function getIDsFromJSON(array $data) {
    $nids = [];
    foreach ($data as $row) {
      $nids[] = $row['nid'];
    }

    return $nids;
  }
}

class RadarMigrateListDrupal7TermServicesJSON extends MigrateListJSON {

  /**
   * {@inheritdoc}
   */
  protected function getIDsFromJSON(array $data) {
    $tids = [];
    foreach ($data as $row) {
      $tids[] = $row['tid'];
    }

    return $tids;
  }
}


class PlotterMigration extends Migration {
  const PLOTTER_NID = 194587;
  const PLOTTER_UID = 3376;

  public function __construct($arguments) {
    parent::__construct($arguments);

    // Everything owned by Plotter. Should be safe for stuff that doesn't have a
    // uid right?
    $this->addFieldMapping('uid')
      ->defaultValue(self::PLOTTER_UID);
  }

  protected function flattenField($values, $key) {
    $flattened = [];
    if (is_array($values)) {
      foreach ($values as $value) {
        $flattened[] = $value->{$key};
      }
    }
    return $flattened;
  }

  protected function flattenImageField($values) {
    $urls = $titles = $alts = [];
    if (is_array($values)) {
      foreach ($values as $value) {
        $urls[] = "https://plotter.infoladen.de/sites/default/files/" . file_uri_target($value->uri);
        $titles[] = $value->title;
        $alts[] = $value->alt;
      }
    }
    return [$urls, $titles, $alts];
  }

  protected function flattenFileField($values) {
    $urls = $descriptions = [];
    if (is_array($values)) {
      foreach ($values as $value) {
        $urls[] = "https://plotter.infoladen.de/sites/default/files/" . file_uri_target($value->uri);
        $descriptions[] = $value->description;
      }
    }
    return [$urls, $descriptions];
  }

  protected function flattenUrlField($values) {
    $urls = $titles = [];
    if (is_array($values)) {
      foreach ($values as $value) {
        $urls[] = $value->url;
        $titles[] = $value->title;
      }
    }
    return [$urls, $titles];
  }
}

class PlotterTerminMigration extends PlotterMigration {

  public function __construct($arguments) {
    parent::__construct($arguments);
    $list_url = 'https://plotter.infoladen.de/api/1.0/node.json?parameters[type]=termin';
    //pattern where the files are at
    $item_url = 'https://plotter.infoladen.de/api/1.0/node/:id.json';
    //no options, or you could specify some
    $http_options = array();
    //map for the migration
    $this->map = new MigrateSQLMap($this->machineName,
        array(
          'nid' => array(
            'type' => 'int',
            'not null' => true,
          ),
        ),
        MigrateDestinationNode::getKeySchema()
    );
    $this->source = new MigrateSourceList(
        new RadarMigrateListDrupal7NodeServicesJSON($list_url, $http_options),
        new MigrateItemJSON($item_url, $http_options),
        $this->fields(),
        ['track_changes' => TRUE]
    );
    $this->destination = new MigrateDestinationNode('event');
    $this->addFieldMapping('title', 'title');
    $this->addFieldMapping('body', 'body');
    $this->addFieldMapping('body:format')->defaultValue('rich_text_editor');
    $this->addFieldMapping('language', 'language');
    $this->addFieldMapping('created', 'created');
    $this->addFieldMapping('changed', 'changed');
    $this->addFieldMapping('status', 'status');
    $this->addFieldMapping('field_date_time', 'field_datum:from');
    $this->addFieldMapping('field_date_time:to', 'field_datum:to');
    $this->addFieldMapping('field_date_time:rrule', 'field_datum:rrule');
    $this->addFieldMapping('field_date_time:timezone', 'field_datum:timezone');
    $this->addFieldMapping('og_group_ref', 'field_veranstaltende_gruppe')
      ->sourceMigration('PlotterGruppeMigration');
    // To still do the Migration lookup, but merge the two sources of location.
    // See self::prepare().
    $this->addFieldMapping('field_offline_dupe', 'field_ort')
      ->sourceMigration('PlotterOrtTermMigration');
    $this->addFieldMapping('field_offline', 'field_ort_in_datenbank')
      ->sourceMigration('PlotterOrtMigration');
    $this->addFieldMapping('field_topic', 'field_kategorie')
      ->sourceMigration('PlotterKategorieMigration');
    $this->addFieldMapping('field_topic:source_type')
      ->defaultValue('tid');
    $this->addFieldMapping('field_image', 'field_image');
    $this->addFieldMapping('field_image:file_class')
      ->defaultValue('MigrateFileUri');
    $this->addFieldMapping('field_image:alt', 'field_image:alt');
    $this->addFieldMapping('field_image:title', 'field_image:title');
    $this->addFieldMapping('field_flyer', 'field_pdf_hinzuf_gen');
    $this->addFieldMapping('field_flyer:file_class')
      ->defaultValue('MigrateFileUri');
    $this->addFieldMapping('field_flyer:description', 'field_pdf_hinzuf_gen:description');
     $this->addFieldMapping('field_link', 'field_url');
    $this->addFieldMapping('field_link:title', 'field_url:title');
    $this->addFieldMapping('comment', null)->defaultValue(1);
  }
  
  /**
   * Return the fields.
   *
   * @return array
   */
  function fields() {
// {"vid":"2567",
//  "title":"Atom-Klima-Protest"
//  "nid":"2567",
//  "type":"termin",
//  "language":"de",
//  "created":"1517218018",
//  "changed":"1517218018",
//  "tnid":"0",
//   "translate":"0",
//  "revision_timestamp":"1517218018",
//  "body":{
//    "und":[
//     {
//       "value":"Liebe Freundinnen...\r\n",
//       "summary":"",
//       "format":"filtered_html",
//       "safe_value":"<p>Liebe Freundinnen...</p>\n",
//       "safe_summary":""
//     }
//    ]
//  },
// "field_datum":{
//   "und":[
//     {
//       "value":"2018-02-06 15:00:00",
//       "value2":"2018-02-06 15:00:00",
//       "rrule":null,
//       "timezone":"Europe/Berlin",
//       "timezone_db":"UTC",
//       "date_type":"datetime",
//       "db":{
//         "value":{
//           "granularity":["year","month","day","hour","minute","second","timezone"],
//           "errors":[],
//           "timeOnly":false,
//           "dateOnly":false,
//           "originalTime":"2018-02-06T15:00:00+00:00",
//           "date":"2018-02-06 15:00:00.000000",
//           "timezone_type":3,"timezone":"UTC"
//         },
//         "value2":{
//           "granularity":["year","month","day","hour","minute","second","timezone"],
//           "errors":[],
//           "timeOnly":false,
//           "dateOnly":false,
//           "originalTime":"2018-02-06T15:00:00+00:00",
//           "date":"2018-02-06 15:00:00.000000",
//           "timezone_type":3,"timezone":"UTC"
//         }
//       }
//     }
//   ]
// },
// "field_ort":{
//   "und":[
//     {
//       "tid":"95"
//     }
//   ]
// },
// "field_ort_in_datenbank":{
//   "und":[
//     {
//       "target_id":"35"
//     }
//   ]
// }
// "field_kategorie":{
//   "und":[
//     {
//       "tid":"17"
//     }
//   ]
// },
// "field_veranstalter_in":{
//   "und":[
//     {
//       "value":"aktion ./. arbeitsunrecht",
//       "format":null,
//       "safe_value":"aktion ./. arbeitsunrecht"
//     }
//   ]
// }
// "field_url":{
//   "und":[
//     {
//       "url":"hambacherforst.org",
//       "title":"Hambacher Forst",
//       "attributes":[]
//     }
//   ]
// },
// "field_veranstaltende_gruppe":{
//   "und":[
//     {
//       "target_id":"94"
//     }
//   ]
// }
// "field_image":{
//   "und":[
//     {
//       "fid":"6724",
//       "filename":"afrin_demo_koeln_27_01_2018 ebertplatz 10 uhr.jpg",
//       "uri":"public://afrin_demo_koeln_27_01_2018 ebertplatz 10 uhr.jpg",
//       "filemime":"image/jpeg",
//       "filesize":"37226",
//       "status":"1",
//       "timestamp":"1516992479",
//       "rdf_mapping":[],
//       "alt":"",
//       "title":"",
//       "width":"640",
//       "height":"335"
//     }
//   ]
// },
// "field_pdf_hinzuf_gen":{
//   "und":[
//     {
//       "fid":"6723",
//       "filename":"Flyer Hambi.pdf",
//       "uri":"public://Flyer Hambi.pdf",
//       "filemime":"application/pdf",
//       "filesize":"1003308",
//       "status":"1",
//       "timestamp":"1516823854",
//       "rdf_mapping":[],
//       "display":"1",
//       "description":""
//     }
//   ]
// },
// "rdf_mapping":{
//   "rdftype":[
//     "sioc:Item",
//     "foaf:Document"
//   ],
//   "title":{
//     "predicates":[
//       "dc:title"
//     ]
//   },
//   "created":{
//     "predicates":["dc:date","dc:created"],
//     "datatype":"xsd:dateTime",
//     "callback":"date_iso8601"
//   },
//   "changed":{
//     "predicates":["dc:modified"],
//     "datatype":"xsd:dateTime",
//     "callback":"date_iso8601"
//   },
//   "body":{
//     "predicates":["content:encoded"]
//   },
//   "comment_count":{
//     "predicates":["sioc:num_replies"],
//     "datatype":"xsd:integer"
//   },
//   "last_activity":{
//     "predicates":["sioc:last_activity_date"],
//     "datatype":"xsd:dateTime",
//     "callback":"date_iso8601"
//   }
// },
// "data":null,
// "path":"https://plotter.infoladen.de/node/2567"
// }
    return array(
      'vid' => 'Plotter VID',
      'title' => 'Group title',
      'nid' => 'Plotter NID',
      'type' => 'Content type: gruppe',
      'language' => 'Language code',
      'created' => 'Created timestamp',
      'changed' => 'Changed timestamp',
      'tnid' => 'Plotter Translation NID',
      'translate' => '',
      'body' => 'Body',
      'field_e_mail' => 'E-mail address',
      'field_ort_in_datenbank' => 'Location (ort) NID',
      'field_url' => 'Event external URL',
      'data' => '',
      'path' => 'URL to event page on Plotter',
    );
  }
  
  /**
   * Remap fields as needed.
   *
   * @param Row $row 
   */
  function prepareRow($row) {
    $row->body = $row->body->{LANGUAGE_NONE}[0]->safe_value;
    $ref_fields_to_flatten = ['field_veranstaltende_gruppe', 'field_ort_in_datenbank'];
    foreach ($ref_fields_to_flatten as $field_name) {
      $row->{$field_name} = $this->flattenField($row->{$field_name}->{LANGUAGE_NONE}, 'target_id');
    }
    $tid_fields_to_flatten = ['field_kategorie', 'field_ort'];
    foreach ($tid_fields_to_flatten as $field_name) {
      $row->{$field_name} = $this->flattenField($row->{$field_name}->{LANGUAGE_NONE}, 'tid');
    }
    // The date is in the db timezone. It needs to be entered in timezone.
    $timezone = $row->field_datum->{LANGUAGE_NONE}[0]->timezone_db;
    $start = new DateObject($row->field_datum->{LANGUAGE_NONE}[0]->value, $timezone);
    $start->setTimezone(new DateTimeZone($row->field_datum->{LANGUAGE_NONE}[0]->timezone));
    $row->{'field_datum:from'} = $start->format('Y-m-d H:i');
    $end = new DateObject($row->field_datum->{LANGUAGE_NONE}[0]->value2, $timezone);
    $end->setTimezone(new DateTimeZone($row->field_datum->{LANGUAGE_NONE}[0]->timezone));
    $row->{'field_datum:to'} = $end->format('Y-m-d H:i');
    $row->{'field_datum:rrule'} = $row->field_datum->{LANGUAGE_NONE}[0]->rrule;
    $row->{'field_datum:timezone'} = $row->field_datum->{LANGUAGE_NONE}[0]->timezone;

    // File URLs.
    $file_fields = ['field_image'];
    foreach ($file_fields as $field_name) {
      list($row->{$field_name}, $row->{$field_name . ':title'}, $row->{$field_name . ':alt'}) = $this->flattenImageField($row->{$field_name}->{LANGUAGE_NONE});
    }
    $file_fields = ['field_pdf_hinzuf_gen'];
    foreach ($file_fields as $field_name) {
      list($row->{$field_name}, $row->{$field_name . ':description'}) = $this->flattenFileField($row->{$field_name}->{LANGUAGE_NONE});
    }
    $link_fields = ['field_url'];
    foreach ($link_fields as $field_name) {
      list($row->{$field_name}, $row->{$field_name . ':title'}) = $this->flattenUrlField($row->{$field_name}->{LANGUAGE_NONE});
    }
    $row->field_url[] = $row->path;
    $row->{'field_url:title'}[] = 'Original-Veranstaltung auf Plotter';
  }

  public function prepare(stdClass $node, stdClass $row) {
    // Plotter categories go into topics (free tags), but can also go into
    // radar general categories where they match.
    $lookup_map = [
      75 => 5,
      69 => 24,
      68 => 6,
      67 => 24,
      66 => 1,
      64 => 42,
      61 => 3,
      56 => 7,
      54 => 24,
      53 => 42 ,
      52 => 26,
      14 => 24,
      57 => 956,
      59 => 429,
      63 => 26,
      65 => 7,
      51 => 24,
      50 => 10,
      49 => 24,
      48 => 7,
      47 => 10,
      46 => 24,
      45 => 2,
      44 => 6,
      43 => 42,
      41 => 5,
      37 => 1,
      36 => 7,
      35 => 4,
      34 => 4,
      32 => 10,
      31 => 5,
      30 => 7,
      29 => 7,
      28 => 2,
      27 => 5,
      24 => 25,
      23 => 10,
      15 => 7,
    ];
    foreach ($row->field_kategorie as $kategorie) {
      if (isset($lookup_map[$kategorie])) {
        $node->field_category[LANGUAGE_NONE][] = ['tid' => $lookup_map[$kategorie]];
      }
    }
    if (!empty($node->field_offline_dupe)) {
      foreach((array)$node->field_offline_dupe as $location_target) {
        $node->field_offline[LANGUAGE_NONE][] = ['target_id' => $location_target];
      }
      unset($node->field_offline_dupe);
    }
    // Causes Date Repeat Entity to generate repeating entities.
    // Setting thing as a defaultValue causes it to be null‽
    $node->{DATE_REPEAT_ENTITY_FIELD_CLONE_STATE}[LANGUAGE_NONE][0] = FALSE;

    // Add plotter's group to any that was imported.
    $node->og_group_ref[LANGUAGE_NONE][] = ['target_id' => self::PLOTTER_NID];
  }

  function prepareRollback($nids) {
    module_load_include('inc', 'date_repeat_entity', 'includes/date_repeat_entity.delete');
    foreach ((array)$nids as $nid) {
      $node = node_load($nid);
      $wrapper = entity_metadata_wrapper('node', $node);
      date_repeat_entity_delete_dates($wrapper, 'node', 'event', 'all');
    }
  }
}

class PlotterGruppeMigration extends PlotterMigration {

  public function __construct($arguments) {
    parent::__construct($arguments);
    $list_url = 'https://plotter.infoladen.de/api/1.0/node.json?parameters[type]=gruppe';
    //pattern where the files are at
    $item_url = 'https://plotter.infoladen.de/api/1.0/node/:id.json';
    //no options, or you could specify some
    $http_options = array();
    //map for the migration
    $this->map = new MigrateSQLMap($this->machineName,
        array(
          'nid' => array(
            'type' => 'int',
            'not null' => true,
          ),
        ),
        MigrateDestinationNode::getKeySchema()
    );
    $this->source = new MigrateSourceList(
        new RadarMigrateListDrupal7NodeServicesJSON($list_url, $http_options),
        new MigrateItemJSON($item_url, $http_options),
        $this->fields(),
        ['track_changes' => TRUE]
    );
    $this->destination = new MigrateDestinationNode('group');
    $this->addFieldMapping('title', 'title');
    $this->addFieldMapping('body', 'body');
    $this->addFieldMapping('body:format')->defaultValue('rich_text_editor');
    $this->addFieldMapping('language', 'language');
    $this->addFieldMapping('created', 'created');
    $this->addFieldMapping('changed', 'changed');
    $this->addFieldMapping('status', 'status');
    $this->addFieldMapping('group_group')
      ->defaultValue(1);
    $this->addFieldMapping('field_og_subscribe_settings')
      ->defaultValue('invitation');
    $this->addFieldMapping('comment', null)->defaultValue(1);
  }
  
  /**
   * Return the fields.
   *
   * @return array
   */
  function fields() {
//  {
//    "vid":"94",
//    "title":"Alternative Liste (AL) Uni K\u00f6ln",
//    "log":"",
//    "status":"1",
//    "comment":"0",
//    "promote":"1",
//    "sticky":"0",
//    "nid":"94",
//    "type":"gruppe",
//    "language":"und",
//    "created":"1417197138",
//    "changed":"1417197138",
//    "tnid":"0",
//    "translate":"0",
//    "revision_timestamp":"1417197138",
//    "revision_uid":"1",
//    "body":{
//      "und":[
//        {
//          "value":"\r\n\r\nAlternative... selbstdarstellung.html\r\n",
//          "summary":"",
//          "format":"filtered_html",
//          "safe_value":"<p>Alternative....selbstdarstellung.html</a></p>\n",
//          "safe_summary":""
//        }
//      ]
//    },
//    "field_e_mail":{
//      "und":[
//        {
//          "value":" al-plenum@uni-koeln.de",
//          "format":null,
//          "safe_value":" al-plenum@uni-koeln.de"
//        }
//      ]
//    },
//    "field_url":{
//      "und":[
//        {
//          "url":"http://alunikoeln.blogsport.de/",
//          "title":"",
//          "attributes":[]
//        }
//      ]
//    },
//    "field_ort_in_datenbank":[],
//    "rdf_mapping":{
//      ...
//    },
//    "data":"a:1:{s:7:\"overlay\";i:1;}",
//    "path":"https://plotter.infoladen.de/node/94"
//  }
    return array(
      'vid' => 'Plotter VID',
      'title' => 'Event title',
      'nid' => 'Plotter NID',
      'type' => 'Content type: termin',
      'language' => 'Language code',
      'created' => 'Created timestamp',
      'changed' => 'Changed timestamp',
      'tnid' => 'Plotter Translation NID',
      'translate' => '',
      'body' => 'Body',
      'field_ort_in_datenbank' => 'Location (ort) NID',
      'field_email' => 'Event public email',
      'field_url' => 'Event external URL',
      'path' => 'URL to event page on Plotter',
    );
  }
  
  /**
   * Remap fields as needed.
   *
   * @param Row $row 
   */
  function prepareRow($row) {
    $row->body = $row->body->{LANGUAGE_NONE}[0]->safe_value;
    $row->field_e_mail = $row->field_e_mail->{LANGUAGE_NONE}[0]->safe_value;
    $ref_fields_to_flatten = ['field_ort_in_datenbank'];
    foreach ($ref_fields_to_flatten as $field_name) {
      $row->{$field_name} = $this->flattenField($row->{$field_name}->{LANGUAGE_NONE}, 'target_id');
    }
    $link_fields = ['field_url'];
    foreach ($link_fields as $field_name) {
      list($row->{$field_name}, $row->{$field_name . ':title'}) = $this->flattenUrlField($row->{$field_name}->{LANGUAGE_NONE});
    }
    $row->field_url[] = $row->path;
    $row->{'field_url:title'}[] = 'Original-Veranstaltung auf Plotter';
  }

  public function complete($node, $row) {
    $plotter = node_load(self::PLOTTER_NID);
    $listed = FALSE;
    foreach ((array) $plotter->field_groups_listed[LANGUAGE_NONE] as $listed_group) {
      if ($listed_group['target_id'] == $node->nid) {
        $listed = TRUE;
        break;
      }
    }
    if (!$listed) {
      $plotter->field_groups_listed[LANGUAGE_NONE][] = ['target_id' => $node->nid];
      node_save($plotter);
    }
  }

  protected function createStub($migration, array $source_id) {
    $node = new stdClass();
    $node->title = t('Stub for @id', array('@id' => $source_id[0]));
    $node->body[LANGUAGE_NONE][0]['value'] = t('Stub body');
    $node->type = $this->destination->getBundle();
    $node->uid = 1;
    $node->status = 0;
    node_save($node);
    if (isset($node->nid)) {
      return array($node->nid);
    }
    else {
      return FALSE;
    }
  }
}

class PlotterOrtMigration extends PlotterMigration {

  public function __construct($arguments) {
    parent::__construct($arguments);
    $list_url = 'https://plotter.infoladen.de/api/1.0/node.json?parameters[type]=ort';
    //pattern where the files are at
    $item_url = 'https://plotter.infoladen.de/api/1.0/node/:id.json';
    //no options, or you could specify some
    $http_options = array();
    //map for the migration
    $this->map = new MigrateSQLMap($this->machineName,
        array(
          'nid' => array(
            'type' => 'int',
            'not null' => true,
          ),
        ),
        MigrateDestinationNode::getKeySchema()
    );
    $this->source = new MigrateSourceList(
        new RadarMigrateListDrupal7NodeServicesJSON($list_url, $http_options),
        new MigrateItemJSON($item_url, $http_options),
        $this->fields(),
        ['track_changes' => TRUE]
    );
    $this->destination = new MigrateDestinationEntityAPI('location', 'location');
    $this->addFieldMapping('language', 'language');
    $this->addFieldMapping('created', 'created');
    $this->addFieldMapping('changed', 'changed');
    $this->addFieldMapping('field_address')
      ->defaultValue('DE');
    $this->addFieldMapping('field_address:postal_code', 'field_plz');
    $this->addFieldMapping('field_address:locality', 'field_stadt');
    $this->addFieldMapping('field_address:thoroughfare', 'field_strasze:street');
    $this->addFieldMapping('field_address:premise', 'field_strasze:number');
    $this->addFieldMapping('field_address:name_line', 'title');
    $this->addFieldMapping('field_timezone')
      ->defaultValue('Europe/Berlin');
    $this->addFieldMapping('field_directions', 'field_wegbeschreibung');
    $this->addFieldMapping('title', 'name_address');
  }
  
  /**
   * Return the fields.
   *
   * @return array
   */
  function fields() {
//  {
//    "vid":"515",
//    "title":"Osterinsel - Bauwagenplatz",
//    "log":"",
//    "status":"1",
//    "comment":"0",
//    "promote":"0",
//    "sticky":"0",
//    "nid":"515",
//    "type":"ort",
//    "language":"de",
//    "created":"1427997763",
//    "changed":"1427997763",
//    "tnid":"0",
//    "translate":"0",
//    "revision_timestamp":"1427997763",
//    "body":{
//      "und":[
//        {
//          "value":"\r\nEine kurze...jede Stadt!\r\n",
//          "summary":"",
//          "format":"filtered_html",
//          "safe_value":"<p>Eine kurze...jede Stadt!</p>\n",
//          "safe_summary":""
//        }
//      ]
//    },
//    "field_e_mail":[],
//    "field_url":{
//      "und":[
//        {
//          "url":"http://www.bwp-koeln.de/",
//          "title":"",
//          "attributes":[]
//        }
//      ]
//    },
//    "field_wegbeschreibung":{
//      "und":[
//        {
//          "value":"siehe\r\n  http://www.bwp-koeln.de/anfahrt.php",
//          "format":null,
//          "safe_value":"siehe\r\n  http://www.bwp-koeln.de/anfahrt.php"
//        }
//      ]
//    },
//    "field_strasze":{
//      "und":[
//        {
//          "value":"Stolbergerstr. 90","
//          format":null,
//          "safe_value":"Stolbergerstr. 90"
//        }
//      ]
//    },
//    "field_plz":{
//      "und":[
//        {
//          "value":"50672",
//          "format":null,
//          "safe_value":"50672"
//        }
//      ]
//    }
//    "field_stadt":{
//      "und":[
//        {
//          "value":"K\u00f6ln",
//          "format":null,
//          "safe_value":"K\u00f6ln"
//        }
//      ]
//    },
//    "field_telefon":[],
//    "field__ffnungszeiten":{
//      "und":[
//        {
//          "value":"\u00d6ffnungszeiten: Montags-Samstags ab 18:00 Uhr... statt",
//          "format":null,
//          "safe_value":"\u00d6ffnungszeiten: Montags-Samstags ab 18:00 Uhr... statt"
//        }
//      ]
//    }
//    "rdf_mapping":{
//    ...
//    },
//    "data":null,
//    "path":"https://plotter.infoladen.de/node/515"
//  }
    return array(
      'vid' => 'Plotter VID',
      'title' => 'Location name',
      'nid' => 'Plotter NID',
      'type' => 'Content type: ort',
      'language' => 'Language code',
      'created' => 'Created timestamp',
      'changed' => 'Changed timestamp',
      'tnid' => 'Plotter Translation NID',
      'translate' => '',
      'body' => 'Body',
      'field_e_mail' => 'Published email address',
      'field_url' => 'Location external URL',
      'field_wegbeschreibung' => 'Directions',
      'field_strasze' => 'Street, including number',
      'field_plz' => 'Postcode',
      'field_stadt' => 'City',
      'field_telefon' => 'Telephone',
      'field__ffnungszeiten' => 'Openings Times',
      'data' => '',
      'path' => 'URL to event page on Plotter',
    );
  }
  
  /**
   * Remap fields as needed.
   *
   * @param Row $row 
   */
  function prepareRow($row) {
    $row->body = $row->body->{LANGUAGE_NONE}[0]->safe_value;
    $street = trim($row->field_strasze->{LANGUAGE_NONE}[0]->safe_value);
    $row->field_strasze = $street;
    if ($last_space = mb_strrpos($street, ' ')) {
      $end_of_street = mb_substr($street, $last_space);
      if (preg_match('/[0-9]/', $end_of_street)) {
        $row->{'field_strasze:number'} = trim($end_of_street);
        $row->{'field_strasze:street'} = trim(mb_substr($street, 0, $last_space));
      }
      else {
        $row->{'field_strasze:number'} = '';
        $row->{'field_strasze:street'} = $street;
      }
    }
    else {
      $row->{'field_strasze:number'} = '';
      $row->{'field_strasze:street'} = $street;
    }
    $row->field_plz = $row->field_plz->{LANGUAGE_NONE}[0]->safe_value;
    $row->field_stadt = $row->field_stadt->{LANGUAGE_NONE}[0]->safe_value;
    $row->name_address = substr($row->title . ' ' . $row->field_strasze . ' ' . $row->field_plz . ' ' . $row->field_stadt, 0, 254);
    $row->field_wegbeschreibung = $row->field_wegbeschreibung->{LANGUAGE_NONE}[0]->safe_value;
  }

  protected function createStub($migration, array $source_id) {
    $location = new stdClass();
    $location->title = t('Stub for @id', array('@id' => $source_id[0]));
    $location->body[LANGUAGE_NONE][0]['value'] = t('Stub body');
    $location->type = $this->destination->getBundle();
    $location->uid = 1;
    $location->status = 0;
    entity_save('location', $location);
    if (isset($location->id)) {
      return array($location->id);
    }
    else {
      return FALSE;
    }
  }

  public function prepare(Entity $location, stdClass $row) {
    try {
      if($geo = geocoder('radar_nominatim', $location->field_address[LANGUAGE_NONE][0])) {
        $location->field_map[LANGUAGE_NONE][0] = $geo->out('wkt');
      }
    }
    catch (Exception $e) {
    }
  }
}

class PlotterKategorieMigration extends PlotterMigration {

  public function __construct($arguments) {
    parent::__construct($arguments);
    $list_url = 'https://plotter.infoladen.de/api/1.0/taxonomy_term.json?parameters[vid]=3';
    //pattern where the files are at
    $item_url = 'https://plotter.infoladen.de/api/1.0/taxonomy_term/:id.json';
    //no options, or you could specify some
    $http_options = array();
    //map for the migration
    $this->map = new MigrateSQLMap($this->machineName,
        array(
          'tid' => array(
            'type' => 'int',
            'not null' => true,
          ),
        ),
        MigrateDestinationNode::getKeySchema()
    );
    $this->source = new MigrateSourceList(
        new RadarMigrateListDrupal7TermServicesJSON($list_url, $http_options),
        new MigrateItemJSON($item_url, $http_options),
        $this->fields(),
        ['track_changes' => TRUE]
    );
    $this->destination = new MigrateDestinationTerm('topic');
    $this->addFieldMapping('name', 'name');
    $this->addFieldMapping('description', 'description');
  }
  
  /**
   * Return the fields.
   *
   * @return array
   */
  function fields() {
    //  {
    //    "tid":"17",
    //    "vid":"3",
    //    "name":"Anti-Atom",
    //    "description":"",
    //    "format":"filtered_html",
    //    "weight":"0",
    //    "vocabulary_machine_name":"kategorie",
    //    "rdf_mapping":{
    //      ..
    //    }
    //  }
    return array(
      'tid' => 'Plotter TID',
      'vid' => 'Plotter VID: 3 - Kategorie',
      'vocabulary_machine_name' => 'Vocabulary machine name: Kategorie',
      'name' => 'name',
      'description' => 'description',
    );
  }
  
  /**
   * Remap fields as needed.
   *
   * @param Row $row 
   */
  function prepareRow($row) {
  }

  protected function createStub($migration, array $source_id) {
    $term = new stdClass();
    $term->title = t('Stub for @id', array('@id' => $source_id[0]));
    $term->vocabulary_machine_name = 'topic';
    $term->vid = 3;
    taxonomy_term_save($term);
    if (isset($term->tid)) {
      return array($term->tid);
    }
    else {
      return FALSE;
    }
  }
}

class PlotterOrtTermMigration extends PlotterMigration {

  public function __construct($arguments) {
    parent::__construct($arguments);
    $list_url = 'https://plotter.infoladen.de/api/1.0/taxonomy_term.json?parameters[vid]=3';
    //pattern where the files are at
    $item_url = 'https://plotter.infoladen.de/api/1.0/taxonomy_term/:id.json';
    //no options, or you could specify some
    $http_options = array();
    //map for the migration
    $this->map = new MigrateSQLMap($this->machineName,
        array(
          'tid' => array(
            'type' => 'int',
            'not null' => true,
          ),
        ),
        MigrateDestinationNode::getKeySchema()
    );
    $this->source = new MigrateSourceList(
        new RadarMigrateListDrupal7TermServicesJSON($list_url, $http_options),
        new MigrateItemJSON($item_url, $http_options),
        $this->fields(),
        ['track_changes' => TRUE]
    );
    $this->destination = new MigrateDestinationEntityAPI('location', 'location');
    $this->addFieldMapping('field_address')
      ->defaultValue('DE');
    $this->addFieldMapping('field_address:locality')
      ->defaultValue('Köln');
    $this->addFieldMapping('field_address:name_line', 'name');
    $this->addFieldMapping('title', 'name');
    $this->addFieldMapping('field_directions', 'description');
  }
  
  /**
   * Return the fields.
   *
   * @return array
   */
  function fields() {
    //  {
    //    "tid":"17",
    //    "vid":"3",
    //    "name":"Anti-Atom",
    //    "description":"",
    //    "format":"filtered_html",
    //    "weight":"0",
    //    "vocabulary_machine_name":"kategorie",
    //    "rdf_mapping":{
    //      ..
    //    }
    //  }
    return array(
      'tid' => 'Plotter TID',
      'vid' => 'Plotter VID: 2 - Ort',
      'vocabulary_machine_name' => 'Vocabulary machine name: ort',
      'name' => 'name',
      'description' => 'description',
    );
  }
  
  /**
   * Remap fields as needed.
   *
   * @param Row $row 
   */
  function prepareRow($row) {
    $row->description = $row->description . "\nAutomatically created from https://plotter.infoladen.de/taxonomy/term/" . $row->tid;
  }

  public function prepare(Entity $location, stdClass $row) {
    try {
      if($geo = geocoder('radar_nominatim', $location->field_address[LANGUAGE_NONE][0])) {
        $location->field_map[LANGUAGE_NONE][0] = $geo->out('wkt');
      }
    }
    catch (Exception $e) {
    }
  }

  protected function createStub($migration, array $source_id) {
    $location = new stdClass();
    $location->title = t('Stub for @id', array('@id' => $source_id[0]));
    $location->body[LANGUAGE_NONE][0]['value'] = t('Stub body');
    $location->type = $this->destination->getBundle();
    $location->uid = 1;
    $location->status = 0;
    entity_save('location', $location);
    if (isset($location->id)) {
      return array($location->id);
    }
    else {
      return FALSE;
    }
  }

}
