<?php
/**
 * @file
 * radar_content_moderation.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function radar_content_moderation_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'corrections';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'entityform';
  $view->human_name = 'Corrections';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Correction messages';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'og_context';
  $handler->display->display_options['access']['perm'] = 'update group';
  $handler->display->display_options['access']['group_id_arg'] = '1';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '30';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Field: Entityform Submission: Reason */
  $handler->display->display_options['fields']['field_reason']['id'] = 'field_reason';
  $handler->display->display_options['fields']['field_reason']['table'] = 'field_data_field_reason';
  $handler->display->display_options['fields']['field_reason']['field'] = 'field_reason';
  $handler->display->display_options['fields']['field_reason']['delta_offset'] = '0';
  /* Field: Entityform Submission: Website */
  $handler->display->display_options['fields']['field_website_link']['id'] = 'field_website_link';
  $handler->display->display_options['fields']['field_website_link']['table'] = 'field_data_field_website_link';
  $handler->display->display_options['fields']['field_website_link']['field'] = 'field_website_link';
  /* Field: Entityform Submission: Listings group */
  $handler->display->display_options['fields']['field_listing_group_reference']['id'] = 'field_listing_group_reference';
  $handler->display->display_options['fields']['field_listing_group_reference']['table'] = 'field_data_field_listing_group_reference';
  $handler->display->display_options['fields']['field_listing_group_reference']['field'] = 'field_listing_group_reference';
  $handler->display->display_options['fields']['field_listing_group_reference']['settings'] = array(
    'bypass_access' => 0,
    'link' => 0,
  );
  /* Field: Entityform Submission: Message */
  $handler->display->display_options['fields']['field_message']['id'] = 'field_message';
  $handler->display->display_options['fields']['field_message']['table'] = 'field_data_field_message';
  $handler->display->display_options['fields']['field_message']['field'] = 'field_message';
  /* Sort criterion: Entityform Submission: Date submitted */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'entityform';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Entityform Submission: Group (field_group) */
  $handler->display->display_options['arguments']['field_group_target_id']['id'] = 'field_group_target_id';
  $handler->display->display_options['arguments']['field_group_target_id']['table'] = 'field_data_field_group';
  $handler->display->display_options['arguments']['field_group_target_id']['field'] = 'field_group_target_id';
  $handler->display->display_options['arguments']['field_group_target_id']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['field_group_target_id']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_group_target_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_group_target_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_group_target_id']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['field_group_target_id']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['field_group_target_id']['validate']['type'] = 'node';
  $handler->display->display_options['arguments']['field_group_target_id']['validate_options']['types'] = array(
    'group' => 'group',
    'touring_group' => 'touring_group',
  );
  /* Filter criterion: Entityform Submission: Entityform Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'entityform';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'correction' => 'correction',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;

  /* Display: Group corrections page */
  $handler = $view->new_display('page', 'Group corrections page', 'page');
  $handler->display->display_options['path'] = 'node/%/corrections';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Corrections';
  $handler->display->display_options['menu']['description'] = 'Correction messages';
  $handler->display->display_options['menu']['weight'] = '10';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: Listingsgroup corrections page */
  $handler = $view->new_display('page', 'Listingsgroup corrections page', 'page_1');
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Entityform Submission: Group */
  $handler->display->display_options['fields']['field_group']['id'] = 'field_group';
  $handler->display->display_options['fields']['field_group']['table'] = 'field_data_field_group';
  $handler->display->display_options['fields']['field_group']['field'] = 'field_group';
  $handler->display->display_options['fields']['field_group']['settings'] = array(
    'bypass_access' => 0,
    'link' => 1,
  );
  /* Field: Entityform Submission: Reason */
  $handler->display->display_options['fields']['field_reason']['id'] = 'field_reason';
  $handler->display->display_options['fields']['field_reason']['table'] = 'field_data_field_reason';
  $handler->display->display_options['fields']['field_reason']['field'] = 'field_reason';
  $handler->display->display_options['fields']['field_reason']['delta_offset'] = '0';
  /* Field: Entityform Submission: Website */
  $handler->display->display_options['fields']['field_website_link']['id'] = 'field_website_link';
  $handler->display->display_options['fields']['field_website_link']['table'] = 'field_data_field_website_link';
  $handler->display->display_options['fields']['field_website_link']['field'] = 'field_website_link';
  /* Field: Entityform Submission: Message */
  $handler->display->display_options['fields']['field_message']['id'] = 'field_message';
  $handler->display->display_options['fields']['field_message']['table'] = 'field_data_field_message';
  $handler->display->display_options['fields']['field_message']['field'] = 'field_message';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Entityform Submission: Listings group (field_listing_group_reference) */
  $handler->display->display_options['arguments']['field_listing_group_reference_target_id']['id'] = 'field_listing_group_reference_target_id';
  $handler->display->display_options['arguments']['field_listing_group_reference_target_id']['table'] = 'field_data_field_listing_group_reference';
  $handler->display->display_options['arguments']['field_listing_group_reference_target_id']['field'] = 'field_listing_group_reference_target_id';
  $handler->display->display_options['arguments']['field_listing_group_reference_target_id']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['field_listing_group_reference_target_id']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_listing_group_reference_target_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_listing_group_reference_target_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_listing_group_reference_target_id']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['field_listing_group_reference_target_id']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['field_listing_group_reference_target_id']['validate']['type'] = 'node';
  $handler->display->display_options['arguments']['field_listing_group_reference_target_id']['validate_options']['types'] = array(
    'listings_group' => 'listings_group',
  );
  $handler->display->display_options['path'] = 'node/%/corrections';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Corrections';
  $handler->display->display_options['menu']['description'] = 'Correction messages';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $export['corrections'] = $view;

  $view = new view();
  $view->name = 'radar_content_moderation_reported_nodes';
  $view->description = 'View for flag: Node Abuse';
  $view->tag = 'flag.abuse';
  $view->base_table = 'node';
  $view->human_name = 'Radar reported nodes';
  $view->core = 0;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Defaults */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->display->display_options['title'] = 'Reported content';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'administer nodes';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['distinct'] = TRUE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'views_bulk_operations' => 'views_bulk_operations',
    'type' => 'type',
    'title' => 'title',
    'count' => 'count',
    'last_updated' => 'last_updated',
    'name' => 'name',
    'body' => 'body',
  );
  $handler->display->display_options['style_options']['default'] = 'last_updated';
  $handler->display->display_options['style_options']['info'] = array(
    'views_bulk_operations' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'type' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'count' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'last_updated' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'name' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'body' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['text']['id'] = 'area';
  $handler->display->display_options['empty']['text']['table'] = 'views';
  $handler->display->display_options['empty']['text']['field'] = 'area';
  $handler->display->display_options['empty']['text']['content'] = 'No content has been reported.';
  $handler->display->display_options['empty']['text']['format'] = '1';
  /* Relationship: Flags: inappropriate_node */
  $handler->display->display_options['relationships']['flag_content_rel']['id'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['flag_content_rel']['field'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['label'] = 'abuse_node';
  $handler->display->display_options['relationships']['flag_content_rel']['flag'] = 'inappropriate_node';
  $handler->display->display_options['relationships']['flag_content_rel']['user_scope'] = 'any';
  /* Relationship: Content: Author */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'node';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  $handler->display->display_options['relationships']['uid']['required'] = TRUE;
  /* Relationship: Flags: inappropriate_node counter */
  $handler->display->display_options['relationships']['flag_count_rel']['id'] = 'flag_count_rel';
  $handler->display->display_options['relationships']['flag_count_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['flag_count_rel']['field'] = 'flag_count_rel';
  $handler->display->display_options['relationships']['flag_count_rel']['flag'] = 'inappropriate_node';
  /* Field: Bulk operations: Content */
  $handler->display->display_options['fields']['views_bulk_operations']['id'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['views_bulk_operations']['field'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['display_type'] = '1';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['enable_select_all_pages'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['force_single'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['entity_load_capacity'] = '10';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_operations'] = array(
    'action::radar_content_moderation_delete_node_block_user' => array(
      'selected' => 1,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
      'postpone_processing' => 0,
    ),
    'action::node_unpublish_action' => array(
      'selected' => 1,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
      'postpone_processing' => 0,
    ),
  );
  /* Field: Content: Type */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'node';
  $handler->display->display_options['fields']['type']['field'] = 'type';
  $handler->display->display_options['fields']['type']['exclude'] = TRUE;
  /* Field: Content: Event title */
  $handler->display->display_options['fields']['title_field']['id'] = 'title_field';
  $handler->display->display_options['fields']['title_field']['table'] = 'field_data_title_field';
  $handler->display->display_options['fields']['title_field']['field'] = 'title_field';
  /* Field: Flags: Flag counter */
  $handler->display->display_options['fields']['count']['id'] = 'count';
  $handler->display->display_options['fields']['count']['table'] = 'flag_counts';
  $handler->display->display_options['fields']['count']['field'] = 'count';
  $handler->display->display_options['fields']['count']['relationship'] = 'flag_count_rel';
  $handler->display->display_options['fields']['count']['label'] = '# of Reports';
  /* Field: Flags: Time last flagged */
  $handler->display->display_options['fields']['last_updated']['id'] = 'last_updated';
  $handler->display->display_options['fields']['last_updated']['table'] = 'flag_counts';
  $handler->display->display_options['fields']['last_updated']['field'] = 'last_updated';
  $handler->display->display_options['fields']['last_updated']['relationship'] = 'flag_count_rel';
  $handler->display->display_options['fields']['last_updated']['label'] = 'Last reported';
  $handler->display->display_options['fields']['last_updated']['date_format'] = 'time ago';
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['relationship'] = 'uid';
  $handler->display->display_options['fields']['name']['label'] = 'Author';
  /* Field: Content: Description */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['type'] = 'text_summary_or_trimmed';
  $handler->display->display_options['fields']['body']['settings'] = array(
    'trim_length' => '600',
  );
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = '0';
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['path'] = 'admin/content/reported';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Reported content';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'management';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['tab_options']['title'] = 'Reported content';
  $handler->display->display_options['tab_options']['weight'] = '';
  $export['radar_content_moderation_reported_nodes'] = $view;

  return $export;
}
