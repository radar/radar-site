<?php
/**
 * @file
 * radar_content_moderation.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function radar_content_moderation_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'entityform_type_defaults';
  $strongarm->value = array(
    'data' => array(
      'form_status' => 'ENTITYFORM_OPEN',
      'roles' => array(
        1 => '1',
        5 => '5',
        2 => 0,
        8 => 0,
        6 => 0,
        3 => 0,
        7 => 0,
      ),
      'resubmit_action' => 'new',
      'submission_page_title' => 'Thank You.',
      'submission_text' => array(
        'value' => '',
        'format' => 'rich_text_editor',
      ),
      'submissions_view' => 'entityforms',
      'user_submissions_view' => '',
      'draft_redirect_path' => '',
      'draft_button_text' => 'Save Draft',
      'draft_save_text' => array(
        'value' => '',
        'format' => 'rich_text_editor',
      ),
      'submit_button_text' => 'Send',
      'submit_confirm_msg' => 'Your message has been sent.',
      'your_submissions' => 'Your messagess: [entityform_type:label]',
      'disallow_resubmit_msg' => 'You already submitted this form',
      'delete_confirm_msg' => 'Are you sure you want to delete this Submission for [entityform_type:label]?',
      'page_title_view' => 'Form Submission: [entityform_type:label]',
      'draftable' => 0,
      'preview_page' => 0,
      'submission_show_submitted' => 0,
    ),
  );
  $export['entityform_type_defaults'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_entityform__correction';
  $strongarm->value = array(
    'view_modes' => array(
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'email' => array(
        'custom_settings' => FALSE,
      ),
      'confirmation' => array(
        'custom_settings' => FALSE,
      ),
      'download' => array(
        'custom_settings' => FALSE,
      ),
      'table' => array(
        'custom_settings' => FALSE,
      ),
      'review' => array(
        'custom_settings' => FALSE,
      ),
      'ical' => array(
        'custom_settings' => FALSE,
      ),
      'diff_standard' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'redirect' => array(
          'weight' => '0',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_entityform__correction'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'flag_abuse_flags';
  $strongarm->value = array(
    'inappropriate_node' => 'inappropriate_node',
    'abuse_user' => 0,
    'abuse_node' => 0,
    'abuse_comment' => 0,
    'notification_groups_all' => 0,
    'abuse_whitelist_user' => 0,
    'abuse_whitelist_comment' => 0,
    'abuse_whitelist_node' => 0,
  );
  $export['flag_abuse_flags'] = $strongarm;

  return $export;
}
