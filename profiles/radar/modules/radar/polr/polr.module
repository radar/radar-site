<?php

/**
 * @file
 *  For now just adds largely unused polr fields type.
 *
 *  Intention to extend to use polr API for custom shortening.
 *  Also views / facet search integration.
 */

/**
 * Implements hook_field_info().
 */
function polr_field_info() {
  return array(
    'polr_text' => array(
      'label' => t('Polr'),
      'description' => t('Short URL.'),
      'default_widget' => 'polr_textfield',
      'default_formatter' => 'polr_default',
    ),
  );
}

/**
 * Implements hook_field_settings_form().
 */
function polr_field_settings_form($field, $instance, $has_data) {
  $settings = $field['settings'];
  $form = [];
  return $form;
}

/**
 * Implements hook_field_instance_settings_form().
 */
function polr_field_instance_settings_form($field, $instance) {
  $settings = $instance['settings'];
  $form = [];
  return $form;
}

/**
 * Implements hook_field_load().
 */
function polr_field_load($entity_type, $entities, $field, $instances, $langcode, &$items, $age) {
  // Always have an item, which we can default.
  foreach ($entities as $id => $entity) {
    if (empty($items[$id])) {
      $items[$id][] = NULL;
    }
  }
}

/**
 * Implements hook_field_validate().
 *
 * Possible error codes:
 * - 'text_value_max_length': The value exceeds the maximum length.
 * - 'text_summary_max_length': The summary exceeds the maximum length.
 */
function polr_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  foreach ($items as $delta => $item) {
    // API check here.
  }
}

/**
 * Implements hook_field_is_empty().
 */
function polr_field_is_empty($item, $field) {
  // We add default value so it's never empty.
  return FALSE;
}

/**
 * Implements hook_field_formatter_info().
 */
function polr_field_formatter_info() {
  return array(
    'polr_default' => array(
      'label' => t('Default'),
      'field types' => array('polr_text'),
    ),
  );
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function polr_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $element = [];
  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function polr_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $summary = '';
  return $summary;
}

/**
 * Implements hook_field_formatter_view().
 */
function polr_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  $glyph = drupal_get_path('module', 'polr') . '/clip.svg';
  switch ($display['type']) {
    case 'polr_default':
    case 'text_trimmed':
      foreach ($items as $delta => $item) {
        if (!empty($item['value'])) {
          $link = 'https://squ.at/' . check_plain($item['value']);
          $element[$delta] = theme('clipboardjs', ['text' => $link, 'alert_style' => 'tooltip', 'alert_text' => t('Copied'), 'button_label' => t('Click to copy')]);
          $element[$delta]['button']['#type'] = 'image_button';
          $element[$delta]['button']['#src'] = $glyph;
        }
      }
      if (empty($element)) {
        $link = 'https://squ.at/r/' . base_convert($entity->nid, 10, 36);
        $element[0] = theme('clipboardjs', ['text' => $link, 'alert_style' => 'tooltip', 'alert_text' => t('Copied'), 'button_label' => t('Click to copy')]);
        $element[0]['button']['#type'] = 'image_button';
        $element[0]['button']['#src'] = $glyph;
      }
      break;
  }
  // Load clipboard.js library.
  libraries_load('clipboard');
  return $element;
}

/**
 * Implements hook_field_widget_info().
 */
function polr_field_widget_info() {
  return array(
    'polr_textfield' => array(
      'label' => t('Polr'),
      'field types' => array('polr_text'),
      'settings' => [],
    ),
  );
}

/**
 * Implements hook_field_widget_settings_form().
 */
function polr_field_widget_settings_form($field, $instance) {
  $widget = $instance['widget'];
  $settings = $widget['settings'];

  $form = [];
  return $form;
}

/**
 * Implements hook_field_widget_form().
 */
function polr_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {

  switch ($instance['widget']['type']) {
    case 'polr_textfield':
      $element['value'] = $element + array(
        '#field_prefix' => '<span>https://squ.at/</span>',
        '#type' => 'textfield',
        '#default_value' => isset($items[$delta]['value']) ? $items[$delta]['value'] : NULL,
        '#size' => 60,
        '#maxlength' => 255,
        '#attributes' => array('class' => array('polr-text')),
        // @todo API integration.
        '#disabled' => TRUE,
      );
      break;
  }

  return $element;
}
