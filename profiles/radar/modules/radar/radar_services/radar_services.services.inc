<?php

/**
 * @file
 * radar_services.services.inc
 */

/**
 * Implements hook_default_services_endpoint().
 */
function radar_services_default_services_endpoint() {
  $export = array();

  $endpoint = new stdClass();
  $endpoint->disabled = FALSE; /* Edit this to true to make a default endpoint disabled initially */
  $endpoint->api_version = 3;
  $endpoint->name = 'api_1_0';
  $endpoint->server = 'rest_server';
  $endpoint->path = 'api/1.0';
  $endpoint->authentication = array(
    'services' => 'services',
  );
  $endpoint->server_settings = array();
  $endpoint->resources = array(
    'entity_file' => array(
      'alias' => 'file',
    ),
    'entity_location' => array(
      'alias' => 'location',
    ),
    'entity_node' => array(
      'alias' => 'node',
    ),
    'search_api' => array(
      'alias' => 'search',
      'operations' => array(
        'retrieve' => array(
          'enabled' => '1',
        ),
      ),
    ),
    'entity_taxonomy_term' => array(
      'alias' => 'taxonomy_term',
    ),
    'entity_user' => array(
      'alias' => 'user',
    ),
  );
  $endpoint->debug = 0;
  $export['api_1_0'] = $endpoint;

  $endpoint = new stdClass();
  $endpoint->disabled = FALSE; /* Edit this to true to make a default endpoint disabled initially */
  $endpoint->api_version = 3;
  $endpoint->name = 'api_1_1';
  $endpoint->server = 'rest_server';
  $endpoint->path = 'api/1.1';
  $endpoint->authentication = array(
    'services' => 'services',
  );
  $endpoint->server_settings = array();
  $endpoint->resources = array(
    'entity_file' => array(
      'alias' => 'file',
      'operations' => array(
        'retrieve' => array(
          'enabled' => '1',
        ),
      ),
    ),
    'entity_location' => array(
      'alias' => 'location',
    ),
    'entity_node' => array(
      'alias' => 'node',
    ),
    'search_api_1_1' => array(
      'alias' => 'search',
      'operations' => array(
        'retrieve' => array(
          'enabled' => '1',
        ),
      ),
    ),
    'entity_taxonomy_term' => array(
      'alias' => 'taxonomy_term',
      'operations' => array(
        'retrieve' => array(
          'enabled' => '1',
        ),
      ),
    ),
    'entity_user' => array(
      'alias' => 'user',
      'actions' => array(
        'login' => array(
          'enabled' => '1',
        ),
        'logout' => array(
          'enabled' => '1',
        ),
        'token' => array(
          'enabled' => '1',
        ),
      ),
    ),
  );
  $endpoint->debug = 0;
  $export['api_1_1'] = $endpoint;

  $endpoint = new stdClass();
  $endpoint->disabled = FALSE; /* Edit this to true to make a default endpoint disabled initially */
  $endpoint->api_version = 3;
  $endpoint->name = 'api_1_2';
  $endpoint->server = 'rest_server';
  $endpoint->path = 'api/1.2';
  $endpoint->authentication = array(
    'services' => 'services',
  );
  $endpoint->server_settings = array();
  $endpoint->resources = array(
    'entity_file' => array(
      'alias' => 'file',
      'operations' => array(
        'retrieve' => array(
          'enabled' => '1',
        ),
        'create' => array(
          'enabled' => '1',
        ),
      ),
    ),
    'entity_location' => array(
      'alias' => 'location',
      'operations' => array(
        'create' => array(
          'enabled' => '1',
        ),
        'retrieve' => array(
          'enabled' => '1',
        ),
      ),
    ),
    'entity_node' => array(
      'alias' => 'node',
      'operations' => array(
        'create' => array(
          'enabled' => '1',
        ),
        'retrieve' => array(
          'enabled' => '1',
        ),
        'update' => array(
          'enabled' => '1',
        ),
      ),
    ),
    'search_api_1_2' => array(
      'alias' => 'search',
      'operations' => array(
        'retrieve' => array(
          'enabled' => '1',
        ),
      ),
    ),
    'unpublished_1_2' => array(
      'alias' => 'unpublished',
      'operations' => array(
        'retrieve' => array(
          'enabled' => '1',
        ),
      ),
    ),
    'entity_taxonomy_term' => array(
      'alias' => 'taxonomy_term',
      'operations' => array(
        'create' => array(
          'enabled' => '1',
        ),
        'retrieve' => array(
          'enabled' => '1',
        ),
        'update' => array(
          'enabled' => '1',
        ),
      ),
    ),
    'entity_user' => array(
      'alias' => 'user',
      'actions' => array(
        'login' => array(
          'enabled' => '1',
        ),
        'logout' => array(
          'enabled' => '1',
        ),
        'token' => array(
          'enabled' => '1',
        ),
      ),
    ),
  );
  $endpoint->debug = 0;
  $export['api_1_2'] = $endpoint;

  return $export;
}
