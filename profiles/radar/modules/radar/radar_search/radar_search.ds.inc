<?php

/**
 * @file
 * radar_search.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function radar_search_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|event|search_index';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'event';
  $ds_fieldsetting->view_mode = 'search_index';
  $ds_fieldsetting->settings = array(
    'field_category' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_expert',
          'lb-el' => 'h3',
          'lb-cl' => 'category',
        ),
      ),
    ),
    'field_date_time' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_expert',
          'lb-el' => 'h2',
          'lb-cl' => 'date',
        ),
      ),
    ),
    'field_topic' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_expert',
          'lb-el' => 'h4',
        ),
      ),
    ),
    'title_field' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_expert',
          'lb-el' => 'h1',
          'lb-cl' => 'title',
        ),
      ),
    ),
  );
  $export['node|event|search_index'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function radar_search_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|event|search_index';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'event';
  $ds_layout->view_mode = 'search_index';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title_field',
        1 => 'field_date_time',
        2 => 'body',
        3 => 'field_category',
        4 => 'field_topic',
        5 => 'field_price_category',
        6 => 'field_offline',
        7 => 'og_group_ref',
        8 => 'field_image',
        9 => 'field_price',
        10 => 'field_phone',
        11 => 'field_link',
        12 => 'field_email',
        13 => 'field_event_status',
      ),
    ),
    'fields' => array(
      'title_field' => 'ds_content',
      'field_date_time' => 'ds_content',
      'body' => 'ds_content',
      'field_category' => 'ds_content',
      'field_topic' => 'ds_content',
      'field_price_category' => 'ds_content',
      'field_offline' => 'ds_content',
      'og_group_ref' => 'ds_content',
      'field_image' => 'ds_content',
      'field_price' => 'ds_content',
      'field_phone' => 'ds_content',
      'field_link' => 'ds_content',
      'field_email' => 'ds_content',
      'field_event_status' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|event|search_index'] = $ds_layout;

  return $export;
}

/**
 * Implements hook_ds_view_modes_info().
 */
function radar_search_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'search_index';
  $ds_view_mode->label = 'Search index';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['search_index'] = $ds_view_mode;

  return $export;
}
