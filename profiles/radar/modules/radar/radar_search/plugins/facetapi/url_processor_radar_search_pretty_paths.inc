<?php

/**
 * @file
 * Extends Pretty Paths to support rest server urls.
 */

/**
 * Small override of FacetapiUrlProcessorPrettyPaths.
 */
class FacetapiUrlProcessorPrettyPathsRadar extends FacetapiUrlProcessorPrettyPaths {
  /**
   * Allows pretty path to be in a query string.
   *
   * Overrides FacetapiUrlProcessorPrettyPaths::fetchParams().
   */
  public function fetchParams() {
    // @todo do this properly and directly populate the values!
    // This is good to test, but composing and then deconstructing seems a waste.
    $original_q = $_GET['q'];
    if (!empty($_GET['facets'])) {
      $facets = array();
      foreach ($_GET['facets'] as $key => $value_array) {
        foreach ($value_array as $value) {
          $facets[] = $key . '/' . $value;
        }
      }
      if (count($facets)) {
        $_GET['q'] .= '/' . implode('/', $facets);
      }
    }
    $params = parent::fetchParams();
    $_GET['q'] = $original_q;

    if (substr(current_path(),0,4) != 'api/' && !user_is_logged_in() && count($this->pathSegments) > 2) {
      drupal_set_message(t('Please log in to filter by more than two facets'));
      drupal_access_denied();
      module_invoke_all('exit');
      drupal_exit();
    }

    return $params;
  }

  public function getQueryString(array $facet, array $values, $active) {
    $params = parent::getQueryString($facet, $values, $active);
    if (!user_is_logged_in() && count($this->pathSegments) >= 2 && !$active) {
      $params['destination'] = parent::getFacetPath($facet, $values, $active);
      $params['login_reason'] = 'facets';
    }
    return $params;
  }

  public function getFacetPath(array $facet, array $values, $active) {
    if (!user_is_logged_in() && count($this->pathSegments) >= 2 && !$active) {
      $path = 'user/login';
    }
    else {
      $path = parent::getFacetPath($facet, $values, $active);
    }

    return $path;
  }
}
