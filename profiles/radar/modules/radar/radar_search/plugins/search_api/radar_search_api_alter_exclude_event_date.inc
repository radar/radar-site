<?php

/**
 * @file
 * Contains the RadarSearchApiAlterExcludeEventDate class.
 */

/**
 * Exclude unpublished nodes from node indexes.
 */
class RadarSearchApiAlterExcludeEventDate extends SearchApiAbstractAlterCallback {

  /**
   * Check whether this data-alter callback is applicable for a certain index.
   *
   * Returns TRUE only for indexes on nodes.
   *
   * @param SearchApiIndex $index
   *   The index to check for.
   *
   * @return boolean
   *   TRUE if the callback can run on the given index; FALSE otherwise.
   */
  public function supportsIndex(SearchApiIndex $index) {
    return $index->getEntityType() === 'node';
  }

  /**
   * Alter items before indexing.
   *
   * Items which are removed from the array won't be indexed, but will be marked
   * as clean for future indexing.
   *
   * @param array $items
   *   An array of items to be altered, keyed by item IDs.
   */
  public function alterItems(array &$items) {
    $yesterday = new DateObject('yesterday');
    foreach ($items as $nid => &$item) {
      $end = new DateObject($item->field_date_time[LANGUAGE_NONE][0]['value2'], $item->field_date_time[LANGUAGE_NONE][0]['timezone']);
      if ($end < $yesterday) {
        unset($items[$nid]);
      }
    }
  }

}
