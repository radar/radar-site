<?php

/**
 * @file
 * Radar location selection handler.
 */

class RadarLocationSelectionHandler extends EntityReference_SelectionHandler_Generic {

  public static function getInstance($field, $instance = NULL, $entity_type = NULL, $entity = NULL) {
    return new self($field, $instance, $entity_type, $entity);
  }

  public function entityFieldQueryAlter(SelectQueryInterface $query) {
    $query->addExpression('LENGTH({title})', 'title_length');
    $query->orderBy('title_length');
  }

}
