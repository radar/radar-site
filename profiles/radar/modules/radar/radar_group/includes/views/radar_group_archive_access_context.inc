<?php

/**
 * @file
 * Contains views access plugin for OG permissions
 */

/**
 * Allow views to allow access to only users with a specified permission in the
 * current group.
 */
class radar_group_archive_access_context extends views_plugin_access {

  /**
   * Return a string to display as the clickable title for the
   * access control.
   */
  function summary_title() {
    return t('Radar group archive enabled');
  }

  /**
   * Determine if the current user has access or not.
   */
  function access($account) {
    _radar_group_archive_access_enabled();
  }

  /**
   * Determine the access callback and arguments.
   */
  function get_access_callback() {
    return ['_radar_group_archive_access_enabled', []];
  }
}
