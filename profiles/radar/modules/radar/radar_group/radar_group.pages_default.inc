<?php

/**
 * @file
 * radar_group.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function radar_group_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view__distributed-group';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = -2;
  $handler->conf = array(
    'title' => 'Distributed group',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'group' => 'group',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
        1 => array(
          'name' => 'entity_field_value:node:group:field_stamping_ground',
          'settings' => array(
            'field_stamping_ground' => array(
              'und' => array(
                0 => array(
                  'tid' => '1416',
                ),
              ),
            ),
            'field_stamping_ground_tid' => array(
              0 => '1416',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
    'metatag_panels' => array(
      'enabled' => 1,
      'metatags' => array(
        'title' => array(
          'value' => '[node:title], [node:field-offline:0:field-address:locality] [node:field-offline:0:field-address:country] | [site:name]',
        ),
        'description' => array(
          'value' => 'Events at [node:title]. [node:summary]',
        ),
        'rating' => array(
          'value' => 'general',
        ),
        'referrer' => array(
          'value' => 'no-referrer',
        ),
        'shortlink' => array(
          'value' => '[node:field_short_url]',
        ),
        'geo.position' => array(
          'value' => '[node:field-offline:0:field-map:lat],[node:field-offline:0:field-map:lon]',
        ),
      ),
    ),
    'name' => 'distributed-group',
  );
  $display = new panels_display();
  $display->layout = 'two_66_33';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'left' => NULL,
      'right' => NULL,
      'two_33_66_top' => NULL,
      'two_33_66_first' => NULL,
      'two_33_66_second' => NULL,
      'two_33_66_bottom' => NULL,
      'two_66_33_top' => NULL,
      'two_66_33_first' => NULL,
      'two_66_33_second' => NULL,
      'two_66_33_bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '38820a9a-b71f-4cf3-a8bf-5f3a87dac8fb';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'node_view__distributed-group';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-5c8cb8f0-7763-42be-b556-1e1d77f15fae';
  $pane->panel = 'two_66_33_first';
  $pane->type = 'node_title';
  $pane->subtype = 'node_title';
  $pane->shown = FALSE;
  $pane->access = array();
  $pane->configuration = array(
    'link' => 0,
    'markup' => 'h2',
    'id' => '',
    'class' => 'group',
    'context' => 'argument_entity_id:node_1',
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'tight',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '5c8cb8f0-7763-42be-b556-1e1d77f15fae';
  $display->content['new-5c8cb8f0-7763-42be-b556-1e1d77f15fae'] = $pane;
  $display->panels['two_66_33_first'][0] = 'new-5c8cb8f0-7763-42be-b556-1e1d77f15fae';
  $pane = new stdClass();
  $pane->pid = 'new-23ad67b7-424b-4a8d-aea4-b1cb4ca8194e';
  $pane->panel = 'two_66_33_first';
  $pane->type = 'node_links';
  $pane->subtype = 'node_links';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Translations',
    'build_mode' => 'full',
    'identifier' => '',
    'link' => 0,
    'context' => 'argument_entity_id:node_1',
    'override_title_heading' => 'div',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'translation-links',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '23ad67b7-424b-4a8d-aea4-b1cb4ca8194e';
  $display->content['new-23ad67b7-424b-4a8d-aea4-b1cb4ca8194e'] = $pane;
  $display->panels['two_66_33_first'][1] = 'new-23ad67b7-424b-4a8d-aea4-b1cb4ca8194e';
  $pane = new stdClass();
  $pane->pid = 'new-2a99011c-a7ad-414c-987e-75ad2fda1b77';
  $pane->panel = 'two_66_33_first';
  $pane->type = 'node_content';
  $pane->subtype = 'node_content';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'links' => 0,
    'no_extras' => 0,
    'override_title' => 0,
    'override_title_text' => '',
    'identifier' => 'group',
    'link' => 0,
    'leave_node_title' => 0,
    'build_mode' => 'full',
    'context' => 'argument_entity_id:node_1',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '2a99011c-a7ad-414c-987e-75ad2fda1b77';
  $display->content['new-2a99011c-a7ad-414c-987e-75ad2fda1b77'] = $pane;
  $display->panels['two_66_33_first'][2] = 'new-2a99011c-a7ad-414c-987e-75ad2fda1b77';
  $pane = new stdClass();
  $pane->pid = 'new-9f746489-7284-4f48-a86e-9b97225a3019';
  $pane->panel = 'two_66_33_first';
  $pane->type = 'views_panes';
  $pane->subtype = 'radar_distributed_event_locations-panel_pane_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = '9f746489-7284-4f48-a86e-9b97225a3019';
  $display->content['new-9f746489-7284-4f48-a86e-9b97225a3019'] = $pane;
  $display->panels['two_66_33_first'][3] = 'new-9f746489-7284-4f48-a86e-9b97225a3019';
  $pane = new stdClass();
  $pane->pid = 'new-17ab2b05-3539-44d5-b53c-0667a0e8e20f';
  $pane->panel = 'two_66_33_second';
  $pane->type = 'node_create_links';
  $pane->subtype = 'node_create_links';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'types' => 'event',
    'field_name' => 'og_group_ref',
    'context' => 'argument_entity_id:node_1',
    'override_title' => 1,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'create-event button',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '17ab2b05-3539-44d5-b53c-0667a0e8e20f';
  $display->content['new-17ab2b05-3539-44d5-b53c-0667a0e8e20f'] = $pane;
  $display->panels['two_66_33_second'][0] = 'new-17ab2b05-3539-44d5-b53c-0667a0e8e20f';
  $pane = new stdClass();
  $pane->pid = 'new-74586fbe-b0b1-438f-b239-bb483af4f2aa';
  $pane->panel = 'two_66_33_second';
  $pane->type = 'views_panes';
  $pane->subtype = 'radar_group_events-panel_pane_3';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '74586fbe-b0b1-438f-b239-bb483af4f2aa';
  $display->content['new-74586fbe-b0b1-438f-b239-bb483af4f2aa'] = $pane;
  $display->panels['two_66_33_second'][1] = 'new-74586fbe-b0b1-438f-b239-bb483af4f2aa';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['node_view__distributed-group'] = $handler;

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view__panel_context_85f5c063-626a-47fa-9356-02655a131e0d';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = -3;
  $handler->conf = array(
    'title' => 'Inactive group',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => 'group-inactive',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => '',
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'group' => 'group',
              'listings_group' => 'listings_group',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
        1 => array(
          'name' => 'entity_field_value:node:group:field_active',
          'settings' => array(
            'field_active' => array(
              'und' => array(
                0 => array(
                  'value' => 1,
                ),
              ),
            ),
            'field_active_value' => array(
              0 => 1,
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'one';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'one_main' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Inactive: %node:title';
  $display->uuid = 'b03553dd-5529-4774-b8c4-fc1f39c6a6ce';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'node_view__panel_context_85f5c063-626a-47fa-9356-02655a131e0d';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-d9401372-980e-4b57-8292-8717a199b53a';
  $pane->panel = 'one_main';
  $pane->type = 'node_content';
  $pane->subtype = 'node_content';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'links' => 0,
    'no_extras' => 0,
    'override_title' => 0,
    'override_title_text' => '',
    'identifier' => 'group',
    'link' => 0,
    'leave_node_title' => 0,
    'build_mode' => 'full',
    'context' => 'argument_entity_id:node_1',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'd9401372-980e-4b57-8292-8717a199b53a';
  $display->content['new-d9401372-980e-4b57-8292-8717a199b53a'] = $pane;
  $display->panels['one_main'][0] = 'new-d9401372-980e-4b57-8292-8717a199b53a';
  $pane = new stdClass();
  $pane->pid = 'new-91103c9a-cb72-40e8-a398-d8bb3ec53aef';
  $pane->panel = 'one_main';
  $pane->type = 'views_panes';
  $pane->subtype = 'radar_locations-panel_pane_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => array(
      0 => 'argument_entity_id:node_1',
    ),
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '91103c9a-cb72-40e8-a398-d8bb3ec53aef';
  $display->content['new-91103c9a-cb72-40e8-a398-d8bb3ec53aef'] = $pane;
  $display->panels['one_main'][1] = 'new-91103c9a-cb72-40e8-a398-d8bb3ec53aef';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-d9401372-980e-4b57-8292-8717a199b53a';
  $handler->conf['display'] = $display;
  $export['node_view__panel_context_85f5c063-626a-47fa-9356-02655a131e0d'] = $handler;

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view__tour-group';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = -1;
  $handler->conf = array(
    'title' => 'Tour group',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'group' => 'group',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
        1 => array(
          'name' => 'entity_field_value:node:group:field_stamping_ground',
          'settings' => array(
            'field_stamping_ground' => array(
              'und' => array(
                0 => array(
                  'tid' => '1231',
                ),
              ),
            ),
            'field_stamping_ground_tid' => array(
              0 => '1231',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
    'metatag_panels' => array(
      'enabled' => 1,
      'metatags' => array(
        'title' => array(
          'value' => '[node:title], [node:field-offline:0:field-address:locality] [node:field-offline:0:field-address:country] | [site:name]',
        ),
        'description' => array(
          'value' => 'Events at [node:title]. [node:summary]',
        ),
        'rating' => array(
          'value' => 'general',
        ),
        'referrer' => array(
          'value' => 'no-referrer',
        ),
        'shortlink' => array(
          'value' => '[node:field_short_url]',
        ),
        'geo.position' => array(
          'value' => '[node:field-offline:0:field-map:lat],[node:field-offline:0:field-map:lon]',
        ),
      ),
    ),
    'name' => 'tour-group',
  );
  $display = new panels_display();
  $display->layout = 'two_66_33';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'left' => NULL,
      'right' => NULL,
      'two_33_66_top' => NULL,
      'two_33_66_first' => NULL,
      'two_33_66_second' => NULL,
      'two_33_66_bottom' => NULL,
      'two_66_33_top' => NULL,
      'two_66_33_first' => NULL,
      'two_66_33_second' => NULL,
      'two_66_33_bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '38820a9a-b71f-4cf3-a8bf-5f3a87dac8fb';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'node_view__tour-group';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-5c8cb8f0-7763-42be-b556-1e1d77f15fae';
  $pane->panel = 'two_66_33_first';
  $pane->type = 'node_title';
  $pane->subtype = 'node_title';
  $pane->shown = FALSE;
  $pane->access = array();
  $pane->configuration = array(
    'link' => 0,
    'markup' => 'h2',
    'id' => '',
    'class' => 'group',
    'context' => 'argument_entity_id:node_1',
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'tight',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '5c8cb8f0-7763-42be-b556-1e1d77f15fae';
  $display->content['new-5c8cb8f0-7763-42be-b556-1e1d77f15fae'] = $pane;
  $display->panels['two_66_33_first'][0] = 'new-5c8cb8f0-7763-42be-b556-1e1d77f15fae';
  $pane = new stdClass();
  $pane->pid = 'new-23ad67b7-424b-4a8d-aea4-b1cb4ca8194e';
  $pane->panel = 'two_66_33_first';
  $pane->type = 'node_links';
  $pane->subtype = 'node_links';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Translations',
    'build_mode' => 'full',
    'identifier' => '',
    'link' => 0,
    'context' => 'argument_entity_id:node_1',
    'override_title_heading' => 'div',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'translation-links',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '23ad67b7-424b-4a8d-aea4-b1cb4ca8194e';
  $display->content['new-23ad67b7-424b-4a8d-aea4-b1cb4ca8194e'] = $pane;
  $display->panels['two_66_33_first'][1] = 'new-23ad67b7-424b-4a8d-aea4-b1cb4ca8194e';
  $pane = new stdClass();
  $pane->pid = 'new-2a99011c-a7ad-414c-987e-75ad2fda1b77';
  $pane->panel = 'two_66_33_first';
  $pane->type = 'node_content';
  $pane->subtype = 'node_content';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'links' => 0,
    'no_extras' => 0,
    'override_title' => 0,
    'override_title_text' => '',
    'identifier' => 'group',
    'link' => 0,
    'leave_node_title' => 0,
    'build_mode' => 'full',
    'context' => 'argument_entity_id:node_1',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '2a99011c-a7ad-414c-987e-75ad2fda1b77';
  $display->content['new-2a99011c-a7ad-414c-987e-75ad2fda1b77'] = $pane;
  $display->panels['two_66_33_first'][2] = 'new-2a99011c-a7ad-414c-987e-75ad2fda1b77';
  $pane = new stdClass();
  $pane->pid = 'new-9f746489-7284-4f48-a86e-9b97225a3019';
  $pane->panel = 'two_66_33_first';
  $pane->type = 'views_panes';
  $pane->subtype = 'radar_distributed_event_locations-panel_pane_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = '9f746489-7284-4f48-a86e-9b97225a3019';
  $display->content['new-9f746489-7284-4f48-a86e-9b97225a3019'] = $pane;
  $display->panels['two_66_33_first'][3] = 'new-9f746489-7284-4f48-a86e-9b97225a3019';
  $pane = new stdClass();
  $pane->pid = 'new-17ab2b05-3539-44d5-b53c-0667a0e8e20f';
  $pane->panel = 'two_66_33_second';
  $pane->type = 'node_create_links';
  $pane->subtype = 'node_create_links';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'types' => 'event',
    'field_name' => 'og_group_ref',
    'context' => 'argument_entity_id:node_1',
    'override_title' => 1,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'create-event button',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '17ab2b05-3539-44d5-b53c-0667a0e8e20f';
  $display->content['new-17ab2b05-3539-44d5-b53c-0667a0e8e20f'] = $pane;
  $display->panels['two_66_33_second'][0] = 'new-17ab2b05-3539-44d5-b53c-0667a0e8e20f';
  $pane = new stdClass();
  $pane->pid = 'new-74586fbe-b0b1-438f-b239-bb483af4f2aa';
  $pane->panel = 'two_66_33_second';
  $pane->type = 'views_panes';
  $pane->subtype = 'radar_group_events-panel_pane_3';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '74586fbe-b0b1-438f-b239-bb483af4f2aa';
  $display->content['new-74586fbe-b0b1-438f-b239-bb483af4f2aa'] = $pane;
  $display->panels['two_66_33_second'][1] = 'new-74586fbe-b0b1-438f-b239-bb483af4f2aa';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['node_view__tour-group'] = $handler;

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view_panel_context_2';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Group',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'group' => 'group',
              'touring_group' => 'touring_group',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
    'metatag_panels' => array(
      'enabled' => 1,
      'metatags' => array(
        'title' => array(
          'value' => '[node:title], [node:field-offline:0:field-address:locality] [node:field-offline:0:field-address:country] | [site:name]',
        ),
        'description' => array(
          'value' => 'Events at [node:title]. [node:summary]',
        ),
        'rating' => array(
          'value' => 'general',
        ),
        'referrer' => array(
          'value' => 'no-referrer',
        ),
        'shortlink' => array(
          'value' => '[node:field_short_url]',
        ),
        'geo.position' => array(
          'value' => '[node:field-offline:0:field-map:lat],[node:field-offline:0:field-map:lon]',
        ),
      ),
    ),
  );
  $display = new panels_display();
  $display->layout = 'two_66_33';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'left' => NULL,
      'right' => NULL,
      'two_33_66_top' => NULL,
      'two_33_66_first' => NULL,
      'two_33_66_second' => NULL,
      'two_33_66_bottom' => NULL,
      'two_66_33_top' => NULL,
      'two_66_33_first' => NULL,
      'two_66_33_second' => NULL,
      'two_66_33_bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '38820a9a-b71f-4cf3-a8bf-5f3a87dac8fb';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'node_view_panel_context_2';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-5c8cb8f0-7763-42be-b556-1e1d77f15fae';
  $pane->panel = 'two_66_33_first';
  $pane->type = 'node_title';
  $pane->subtype = 'node_title';
  $pane->shown = FALSE;
  $pane->access = array();
  $pane->configuration = array(
    'link' => 0,
    'markup' => 'h2',
    'id' => '',
    'class' => 'group',
    'context' => 'argument_entity_id:node_1',
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'tight',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '5c8cb8f0-7763-42be-b556-1e1d77f15fae';
  $display->content['new-5c8cb8f0-7763-42be-b556-1e1d77f15fae'] = $pane;
  $display->panels['two_66_33_first'][0] = 'new-5c8cb8f0-7763-42be-b556-1e1d77f15fae';
  $pane = new stdClass();
  $pane->pid = 'new-23ad67b7-424b-4a8d-aea4-b1cb4ca8194e';
  $pane->panel = 'two_66_33_first';
  $pane->type = 'node_links';
  $pane->subtype = 'node_links';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Translations',
    'build_mode' => 'full',
    'identifier' => '',
    'link' => 0,
    'context' => 'argument_entity_id:node_1',
    'override_title_heading' => 'div',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'translation-links',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '23ad67b7-424b-4a8d-aea4-b1cb4ca8194e';
  $display->content['new-23ad67b7-424b-4a8d-aea4-b1cb4ca8194e'] = $pane;
  $display->panels['two_66_33_first'][1] = 'new-23ad67b7-424b-4a8d-aea4-b1cb4ca8194e';
  $pane = new stdClass();
  $pane->pid = 'new-2a99011c-a7ad-414c-987e-75ad2fda1b77';
  $pane->panel = 'two_66_33_first';
  $pane->type = 'node_content';
  $pane->subtype = 'node_content';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'links' => 0,
    'no_extras' => 0,
    'override_title' => 0,
    'override_title_text' => '',
    'identifier' => 'group',
    'link' => 0,
    'leave_node_title' => 0,
    'build_mode' => 'full',
    'context' => 'argument_entity_id:node_1',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '2a99011c-a7ad-414c-987e-75ad2fda1b77';
  $display->content['new-2a99011c-a7ad-414c-987e-75ad2fda1b77'] = $pane;
  $display->panels['two_66_33_first'][2] = 'new-2a99011c-a7ad-414c-987e-75ad2fda1b77';
  $pane = new stdClass();
  $pane->pid = 'new-a0ed1d52-01a5-45be-bed4-f9cacfe7b393';
  $pane->panel = 'two_66_33_first';
  $pane->type = 'views_panes';
  $pane->subtype = 'radar_locations-panel_pane_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => array(
      0 => 'argument_entity_id:node_1',
    ),
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'address-map',
  );
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = 'a0ed1d52-01a5-45be-bed4-f9cacfe7b393';
  $display->content['new-a0ed1d52-01a5-45be-bed4-f9cacfe7b393'] = $pane;
  $display->panels['two_66_33_first'][3] = 'new-a0ed1d52-01a5-45be-bed4-f9cacfe7b393';
  $pane = new stdClass();
  $pane->pid = 'new-17ab2b05-3539-44d5-b53c-0667a0e8e20f';
  $pane->panel = 'two_66_33_second';
  $pane->type = 'node_create_links';
  $pane->subtype = 'node_create_links';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'types' => 'event',
    'field_name' => 'og_group_ref',
    'context' => 'argument_entity_id:node_1',
    'override_title' => 1,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'create-event button',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '17ab2b05-3539-44d5-b53c-0667a0e8e20f';
  $display->content['new-17ab2b05-3539-44d5-b53c-0667a0e8e20f'] = $pane;
  $display->panels['two_66_33_second'][0] = 'new-17ab2b05-3539-44d5-b53c-0667a0e8e20f';
  $pane = new stdClass();
  $pane->pid = 'new-1798fc28-a444-4373-8a51-b06fb894e14d';
  $pane->panel = 'two_66_33_second';
  $pane->type = 'views_panes';
  $pane->subtype = 'radar_group_events-panel_pane_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '1798fc28-a444-4373-8a51-b06fb894e14d';
  $display->content['new-1798fc28-a444-4373-8a51-b06fb894e14d'] = $pane;
  $display->panels['two_66_33_second'][1] = 'new-1798fc28-a444-4373-8a51-b06fb894e14d';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['node_view_panel_context_2'] = $handler;

  return $export;
}
