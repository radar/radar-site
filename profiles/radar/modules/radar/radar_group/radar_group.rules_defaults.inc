<?php

/**
 * @file
 * radar_group.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function radar_group_default_rules_configuration() {
  $items = array();
  $items['radar_group_administrator_notification_pending_group_updated'] = entity_import('rules_config', '{ "radar_group_administrator_notification_pending_group_updated" : {
      "LABEL" : "Administrator notification: pending group is updated",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_update" : [] },
      "IF" : [
        { "NOT node_is_published" : { "node" : [ "node" ] } },
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "group" : "group", "touring_group" : "touring_group" } }
          }
        },
        { "data_is_empty" : { "data" : [ "node:field-rejected" ] } }
      ],
      "DO" : [
        { "mail_to_users_of_role" : {
            "roles" : { "value" : { "3" : "3" } },
            "subject" : "[Radar: Pending] Updated [node:type] \\u0022[node:title]\\u0022 ",
            "message" : "Radar [node:type] \\u0022[node:title]\\u0022 of \\u0022[node:offline:0:title] awaiting approval has been updated.\\r\\n[site:url]\\/node\\/[node:nid]\\r\\n\\r\\n[node:diff-markdown]\\r\\n\\r\\nCheck [site:url]\\/admin\\/content\\/groups"
          }
        }
      ]
    }
  }');
  $items['rules_administrator_notification_groups_all'] = entity_import('rules_config', '{ "rules_administrator_notification_groups_all" : {
      "LABEL" : "Administrator notification: all pending groups",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_insert" : [] },
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "group" : "group", "touring_group" : "touring_group" } }
          }
        },
        { "NOT node_is_published" : { "node" : [ "node" ] } }
      ],
      "DO" : [
        { "mail_to_users_of_role" : {
            "roles" : { "value" : { "3" : "3" } },
            "subject" : "[Radar: Pending] CREATED [node:type] \\u0022[node:title]\\u0022 ",
            "message" : "Radar [node:type] \\u0022[node:title]\\u0022 of \\u0022[node:offline:0:title] awaiting approval has been created.\\r\\n[site:url]\\/node\\/[node:nid]\\r\\n\\r\\n[node:body]\\r\\n\\r\\nCheck [site:url]\\/admin\\/content\\/groups"
          }
        }
      ]
    }
  }');
  $items['rules_notification_published_group'] = entity_import('rules_config', '{ "rules_notification_published_group" : {
      "LABEL" : "Notification about published group",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_update--group" : { "bundle" : "group" } },
      "IF" : [
        { "NOT node_is_published" : { "node" : [ "node-unchanged" ] } },
        { "node_is_published" : { "node" : [ "node" ] } }
      ],
      "DO" : [
        { "mail" : {
            "to" : [ "node:author:mail" ],
            "subject" : "Radar group approved",
            "message" : "The group [node:title] has been approved, and can be found at [node:url]\\r\\nIf you have already created events, then they are visible as well now.",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_unpublish_rejected_group'] = entity_import('rules_config', '{ "rules_unpublish_rejected_group" : {
      "LABEL" : "Unpublish rejected group",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : {
        "node_presave--group" : { "bundle" : "group" },
        "node_presave--touring_group" : { "bundle" : "touring_group" }
      },
      "IF" : [ { "NOT data_is_empty" : { "data" : [ "node:field-rejected" ] } } ],
      "DO" : [ { "node_unpublish" : { "node" : [ "node" ] } } ]
    }
  }');
  return $items;
}
