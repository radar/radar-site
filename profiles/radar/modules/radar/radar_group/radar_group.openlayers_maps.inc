<?php

/**
 * @file
 * radar_group.openlayers_maps.inc
 */

/**
 * Implements hook_openlayers_maps().
 */
function radar_group_openlayers_maps() {
  $export = array();

  $openlayers_maps = new stdClass();
  $openlayers_maps->disabled = FALSE; /* Edit this to true to make a default openlayers_maps disabled initially */
  $openlayers_maps->api_version = 1;
  $openlayers_maps->name = 'radar_distributed_events';
  $openlayers_maps->title = 'Radar distributed events';
  $openlayers_maps->description = 'Map of locations for a group with distributed events.';
  $openlayers_maps->data = array(
    'width' => 'auto',
    'height' => '400px',
    'image_path' => 'profiles/radar/modules/contrib/openlayers/themes/default_dark/img/',
    'css_path' => 'profiles/radar/modules/contrib/openlayers/themes/default_dark/style.css',
    'proxy_host' => '',
    'hide_empty_map' => 1,
    'center' => array(
      'initial' => array(
        'centerpoint' => '8.789062462493126, 49.49667454416194',
        'zoom' => '12',
      ),
      'restrict' => array(
        'restrictextent' => 0,
        'restrictedExtent' => '',
      ),
    ),
    'behaviors' => array(
      'openlayers_behavior_cluster' => array(
        'clusterlayer' => array(
          'radar_distributed_event_locations_openlayers_1' => 'radar_distributed_event_locations_openlayers_1',
        ),
        'distance' => '20',
        'threshold' => '',
        'display_cluster_numbers' => 1,
        'middle_lower_bound' => '15',
        'middle_upper_bound' => '50',
        'low_color' => '#909090',
        'low_stroke_color' => '#909090',
        'low_opacity' => '1',
        'low_point_radius' => '10',
        'low_label_outline' => '1',
        'middle_color' => '#606060',
        'middle_stroke_color' => '#606060',
        'middle_opacity' => '1',
        'middle_point_radius' => '16',
        'middle_label_outline' => '1',
        'high_color' => '#333',
        'high_stroke_color' => '#333',
        'high_opacity' => '1',
        'high_point_radius' => '22',
        'high_label_outline' => '1',
        'label_low_color' => 'white',
        'label_low_opacity' => '1',
        'label_middle_color' => 'white',
        'label_middle_opacity' => '1',
        'label_high_color' => 'white',
        'label_high_opacity' => '1',
      ),
      'openlayers_behavior_attribution' => array(
        'separator' => '',
      ),
      'openlayers_behavior_layerswitcher' => array(
        'ascending' => 1,
        'sortBaseLayer' => '0',
        'roundedCorner' => 1,
        'roundedCornerColor' => '#222222',
        'maximizeDefault' => 0,
        'div' => '',
      ),
      'openlayers_behavior_navigation' => array(
        'zoomWheelEnabled' => 0,
        'zoomBoxEnabled' => 1,
        'documentDrag' => 0,
      ),
      'openlayers_behavior_panzoombar' => array(
        'zoomWorldIcon' => 0,
        'panIcons' => 1,
      ),
      'openlayers_behavior_zoomtolayer' => array(
        'zoomtolayer' => array(
          'radar_distributed_event_locations_openlayers_1' => 'radar_distributed_event_locations_openlayers_1',
          'osm_mapnik' => 0,
          'osm_cycle' => 0,
        ),
        'point_zoom_level' => '12',
        'zoomtolayer_scale' => '1',
      ),
      'openlayers_behavior_popup' => array(
        'layers' => array(
          'radar_distributed_event_locations_openlayers_1' => 'radar_distributed_event_locations_openlayers_1',
        ),
        'popupAtPosition' => 'mouse',
        'panMapIfOutOfView' => 0,
        'keepInMap' => 1,
      ),
    ),
    'default_layer' => 'osm_mapnik',
    'layers' => array(
      'osm_mapnik' => 'osm_mapnik',
      'osm_cycle' => 'osm_cycle',
      'radar_distributed_event_locations_openlayers_1' => 'radar_distributed_event_locations_openlayers_1',
    ),
    'layer_weight' => array(
      'geofield_formatter' => '0',
      'openlayers_kml_example' => '0',
      'openlayers_geojson_picture_this' => '0',
      'radar_locations_openlayers_1' => '0',
      'radar_locations_openlayers_2' => '0',
      'radar_listed_groups_openlayers_1' => '0',
      'radar_group_search_openlayers_1' => '0',
      'radar_distributed_event_locations_openlayers_1' => '0',
    ),
    'layer_styles' => array(
      'geofield_formatter' => '0',
      'openlayers_kml_example' => '0',
      'openlayers_geojson_picture_this' => '0',
      'radar_locations_openlayers_1' => '0',
      'radar_locations_openlayers_2' => '0',
      'radar_listed_groups_openlayers_1' => '0',
      'radar_group_search_openlayers_1' => '0',
      'radar_distributed_event_locations_openlayers_1' => '0',
    ),
    'layer_styles_select' => array(
      'geofield_formatter' => '0',
      'openlayers_kml_example' => '0',
      'openlayers_geojson_picture_this' => '0',
      'radar_locations_openlayers_1' => '0',
      'radar_locations_openlayers_2' => '0',
      'radar_listed_groups_openlayers_1' => '0',
      'radar_group_search_openlayers_1' => '0',
      'radar_distributed_event_locations_openlayers_1' => '0',
    ),
    'layer_styles_temporary' => array(
      'geofield_formatter' => '0',
      'openlayers_kml_example' => '0',
      'openlayers_geojson_picture_this' => '0',
      'radar_locations_openlayers_1' => '0',
      'radar_locations_openlayers_2' => '0',
      'radar_listed_groups_openlayers_1' => '0',
      'radar_group_search_openlayers_1' => '0',
      'radar_distributed_event_locations_openlayers_1' => '0',
    ),
    'layer_activated' => array(
      'radar_distributed_event_locations_openlayers_1' => 'radar_distributed_event_locations_openlayers_1',
      'geofield_formatter' => 0,
      'openlayers_kml_example' => 0,
      'openlayers_geojson_picture_this' => 0,
      'radar_locations_openlayers_1' => 0,
      'radar_locations_openlayers_2' => 0,
      'radar_listed_groups_openlayers_1' => 0,
      'radar_group_search_openlayers_1' => 0,
    ),
    'layer_switcher' => array(
      'radar_distributed_event_locations_openlayers_1' => 0,
      'geofield_formatter' => 0,
      'openlayers_kml_example' => 0,
      'openlayers_geojson_picture_this' => 0,
      'radar_locations_openlayers_1' => 0,
      'radar_locations_openlayers_2' => 0,
      'radar_listed_groups_openlayers_1' => 0,
      'radar_group_search_openlayers_1' => 0,
    ),
    'projection' => 'EPSG:3857',
    'displayProjection' => 'EPSG:4326',
    'styles' => array(
      'default' => 'default_marker_black_small',
      'select' => 'default_marker_black',
      'temporary' => 'default_marker_black',
    ),
  );
  $export['radar_distributed_events'] = $openlayers_maps;

  return $export;
}
